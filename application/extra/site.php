<?php

return array (
  'name' => 'FastAdmin',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.1',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'chexian' => '车险',
    'feichexian' => '非车险',
    'shouxian' => '寿险',
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
    'user' => 'User',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'baoxiantype' => 
  array (
    '私家车' => '私家车',
    '摩托车' => '摩托车',
    '过户私家车' => '过户私家车',
    '非营运火车' => '非营运货车',
    '2T以下火车' => '2T以下货车',
  ),
  'car_insurance' => 
  array (
    'top' => '1.5',
    'second' => '0.6',
  ),
  'car_insurance_not' => 
  array (
    'top' => '2.0',
    'second' => '0.9',
  ),
  'life_insurance' => 
  array (
    'top' => '1.6',
    'second' => '0.8',
  ),
  'default_mobile' => '18108157848',
);