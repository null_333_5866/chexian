<?php

return [
    'Id'  =>  'ID',
    'Title'  =>  '标题',
    'Description_content'  =>  '描述',
    'Image'  =>  '图片链接',
    'Url'  =>  'url地址',
    'Status'  =>  '状态',
    'Base_url'  =>  '根目录'
];
