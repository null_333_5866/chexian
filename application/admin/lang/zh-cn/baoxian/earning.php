<?php

return [
    'Id'  =>  'ID',
    'Sale_mobile'  =>  '销售人员手机号',
    'Customer_name'  =>  '客户姓名',
    'Business_type'  =>  '业务类型',
    'Start_time'  =>  '开始时间',
    'Data_images'  =>  '资料图片',
    'Remark_content'  =>  '备注',
    'Birthday_time'  =>  '客户生日',
    'End_time'  =>  '结束时间',
    'Car_type'  =>  '车型',
    'Company_name'  =>  '公司名称',
    'Commerce_insurance'  =>  '商业险',
    'Traffic_insurance'  =>  '交强险',
    'Tax'  =>  '税',
    'Total_money'  =>  '总费用',
    'Top_mobile'  =>  '一级人员手机号',
    'Top_persent'  =>  '一级百分比',
    'Top_money'  =>  '一级收益',
    'Second_mobile'  =>  '二级人员手机号',
    'Second_persent'  =>  '二级百分比',
    'Second_money'  =>  '二级收益',
    'Earning'  =>  '收益',
    'Earning_persent'  =>  '收益百分比'
];
