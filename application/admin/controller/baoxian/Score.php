<?php

namespace app\admin\controller\baoxian;

use app\common\controller\Backend;

/**
 * 积分管理
 *
 * @icon fa fa-circle-o
 */
class Score extends Backend {

    /**
     * BaoxianScore模型对象
     * @var \app\admin\model\BaoxianScore
     */
    protected $model = null;

    /**
     * @var string
     */
    protected $searchFields = 'id,mobile';

    public function _initialize() {
        parent::_initialize();
        $this->model = model('BaoxianScore');

    }

    /**
     * @return string
     */
    public function add() {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $userModel = model('User');
                $userData = $userModel->where(['mobile' => $params['mobile']])->find();
                if (empty($userData)) {
                    $this->error('手机号不存在');
                }

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = basename(str_replace('\\', '/', get_class($this->model)));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : true) : $this->modelValidate;
                        $this->model->validate($validate);
                    }
                    $score = $params['score'];
                    $params['score'] = -$score;
                    $mobile = $params['mobile'];
                    $params['sale_name'] = $userData['username'];
                    $result = $this->model->allowField(true)->save($params);
                    if ($result !== false) {
                        $userModel->where(['mobile' => $mobile])->setDec('share_score', $score);
                        $userModel->where(['mobile' => $mobile])->setInc('share_used_score', $score);
                        $this->success();
                    } else {
                        $this->error($this->model->getError());
                    }
                } catch (\think\exception\PDOException $e) {
                    $this->error($e->getMessage());
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */


}
