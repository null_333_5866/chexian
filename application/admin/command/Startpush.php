<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 2018/7/15
 * Time: 12:44
 */

namespace app\admin\command;

use app\common\model\BaoxianWechatPush;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use addons\wechat\library\Config as ConfigService;
use EasyWeChat\Factory;

class Startpush extends Command {


    protected function configure() {
        $this->setName('startpush')
            ->setDescription('Here is the start push ');
    }

    /**
     * @param Input $input
     * @param Output $output
     * @return null
     */
    protected function execute(Input $input, Output $output) {

        $list = BaoxianWechatPush::getAllList();
        $app = Factory::officialAccount(ConfigService::load());
        if (!empty($list)) {
            foreach ($list as $value) {
                $params = json_decode($value['params'], true);
                $app->template_message->send($params);
                BaoxianWechatPush::update(['push_time' => time(), 'is_push' => 2], ['id' => $value['id']]);
            }
        }
        $output->info('push success');
    }
}