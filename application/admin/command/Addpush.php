<?php
/**
 * Created by PhpStorm.
 * User: asus
 * Date: 2018/7/15
 * Time: 12:44
 */

namespace app\admin\command;

use app\common\model\BaoxianWechatPush;
use think\console\Command;
use think\console\Input;
use think\console\Input\Option;
use think\console\Output;
use app\common\model\BaoxianEarning;
use app\common\model\User;
use app\common\model\BaoxianPush;

class Addpush extends Command {

    /**
     * @var array
     */
    protected $remindBaoXianConfig = [1, 3, 5, 15, 30];

    /**
     * @var array
     */
    protected $remindBaoXianBirthDayConfig = [1, 3, 5];

    protected $birthdayTemplateId;

    protected function configure() {
        $this->setName('addpush')
            ->addOption('action', 'a', Option::VALUE_REQUIRED, 'action name required', null)
            ->setDescription('Here is the add push ');
    }

    /**
     * @param Input $input
     * @param Output $output
     * @return null
     */
    protected function execute(Input $input, Output $output) {
        $defaultTemplateId = config('site.default_template_id');
        $this->birthdayTemplateId = $defaultTemplateId;
        //push_one 保单到期增加 push_two 生日提醒增加
        $action = $input->getOption('action') ?: '';
        if ($action == 'push_one') {
            $this->baoxianEnd();
            $this->birthday();
        } elseif ($action == 'push_two') {
//            $this->birthday();
        }

        $output->info("add {$action} success");
    }


    /**
     * @return bool
     */
    protected function birthday() {
        $list = User::getHasOpenIdUser();
        $mobiles = [];
        $newList = [];
        if (!empty($list)) {
            foreach ($list as $item) {
                $mobiles[] = $item['mobile'];
                $newList[$item['mobile']] = $item;
            }
        } else {
            return true;
        }

        //获取base url
        $data = BaoxianPush::getPushList();
        if (!empty($data)) {
            $data = $data[0];
            $baseUrl = $data['base_url'];
        } else {
            $baseUrl = 'https://www.baoxianit.cn';
        }

        $listTwo = BaoxianEarning::getDataByMobilesForBirthDay($mobiles);
        $pushData = [];
        if (!empty($listTwo)) {
            foreach ($listTwo as $item) {
                if (!empty($item['birthday_time'])) {
                    $openId = $newList[$item['sale_mobile']]['open_id'];
                    foreach ($this->remindBaoXianBirthDayConfig as $v) {
                        $compareTime = time() + $v * 86400;
                        if (date('md', $compareTime) === date('md', $item['birthday_time'])) {
                            $temp = [
                                'touser' => $openId,
                                'template_id' => $this->birthdayTemplateId,
                                'url' => $baseUrl . '/index/user/earning.html',
                                'data' => [
                                    'first' => "尊敬的工作人员，客户{$item['customer_name']}{$item['business_type']}还有{$v}天生日。",
                                    'keyword1' => date('Y年m月d日', $item['birthday_time']),
                                    'keyword2' => date('Y年m月d日', $item['birthday_time']),
                                    'remark' => '请及时提醒客户续保 若保险时间提醒有误，可重新设置保险时间。'
                                ]
                            ];
                            $pushData[] = [
                                'params' => json_encode($temp),
                                'template_type' => 2,
                                'is_push' => 1,
                                'create_time' => time(),
                                'push_time' => 0
                            ];
                        }
                    }
                }
            }
        }

        if (!empty($pushData)) {
            BaoxianWechatPush::InsertAllPushData($pushData);
        }
    }

    /**
     * @return bool
     */
    protected function baoxianEnd() {
        $list = User::getHasOpenIdUser();
        $mobiles = [];
        $newList = [];
        if (!empty($list)) {
            foreach ($list as $item) {
                $mobiles[] = $item['mobile'];
                $newList[$item['mobile']] = $item;
            }
        } else {
            return true;
        }

        //获取base url
        $data = BaoxianPush::getPushList();
        if (!empty($data)) {
            $data = $data[0];
            $baseUrl = $data['base_url'];
        } else {
            $baseUrl = 'https://www.baoxianit.cn';
        }

        $listTwo = BaoxianEarning::getDataByMobiles($mobiles);
        $pushData = [];
        if (!empty($listTwo)) {
            foreach ($listTwo as $item) {
                if (!empty($item['end_time'])) {
                    $todayTime = strtotime(date('Y-m-d'));
                    $endTime = strtotime(date('Y-m-d', $item['end_time']));
                    $countTime = $endTime - $todayTime;
                    $days = ceil(($countTime / 86400));
                    $openId = $newList[$item['sale_mobile']]['open_id'];
                    foreach ($this->remindBaoXianConfig as $v) {
                        if ($v == $days) {
                            $temp = [
                                'touser' => $openId,
                                'template_id' => $this->birthdayTemplateId,
                                'url' => $baseUrl . '/index/user/earning.html',
                                'data' => [
                                    'first' => "尊敬的工作人员，客户{$item['customer_name']}{$item['business_type']}还有{$v}天到期。",
                                    'keyword1' => date('Y年m月d日', $item['start_time']),
                                    'keyword2' => date('Y年m月d日', $item['end_time']),
                                    'remark' => '请及时提醒客户续保 若保险时间提醒有误，可重新设置保险时间。'
                                ]
                            ];
                            $pushData[] = [
                                'params' => json_encode($temp),
                                'template_type' => 1,
                                'is_push' => 1,
                                'create_time' => time(),
                                'push_time' => 0
                            ];
                        }
                    }
                }
            }
        }

        if (!empty($pushData)) {
            BaoxianWechatPush::InsertAllPushData($pushData);
        }
    }
}