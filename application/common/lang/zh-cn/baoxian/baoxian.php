<?php

return [
    'Baoxian_id'  =>  '保险ID',
    'Company_id'  =>  '公司ID',
    'Company_name' => '公司名称',
    'Type'  =>  '栏目类型',
    'Detail_name'  =>  '险种名称',
    'Rate'  =>  '佣金比例'
];
