<?php

return [
    'Id'  =>  '公司ID',
    'Name'  =>  '公司名称',
    'Avatar'  =>  '公司Logo',
    'Image'  =>  '销售二维码',
    'Mobile'  =>  '咨询电话'
];
