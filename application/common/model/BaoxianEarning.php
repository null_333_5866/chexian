<?php

namespace app\common\model;

use think\Model;

class BaoxianEarning extends Model {
    // 表名
    protected $name = 'baoxian_earning';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;

    // 追加属性
    protected $append = [
        'start_time_text',
        'birthday_time_text',
        'end_time_text'
    ];

    public function getStartTimeTextAttr($value, $data) {
        $value = $value ? $value : $data['start_time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getBirthdayTimeTextAttr($value, $data) {
        $value = $value ? $value : $data['birthday_time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEndTimeTextAttr($value, $data) {
        $value = $value ? $value : $data['end_time'];
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setStartTimeAttr($value) {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setBirthdayTimeAttr($value) {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    protected function setEndTimeAttr($value) {
        return $value && !is_numeric($value) ? strtotime($value) : $value;
    }

    /**
     * @param array $data
     * 业务收益计算
     * @return array
     */
    public function earningCount($data = []) {
        $result = $scoreList = [];
        if (empty($data)) {
            return $result;
        }

        $carInsurance = config('site.car_insurance');
        $carInsuranceNot = config('site.car_insurance_not');
        $lifeInsurance = config('site.life_insurance');
        $defaultMobile = config('site.default_mobile');
        $userModel = model('User');

        foreach ($data as $datum) {
            if ($datum['business_type'] == '车险') {
                $top = $carInsurance['top'];
                $second = $carInsurance['second'];
            } elseif ($datum['business_type'] == '非车险') {
                $top = $carInsuranceNot['top'];
                $second = $carInsuranceNot['second'];
            } else {
                $top = $lifeInsurance['top'];
                $second = $lifeInsurance['second'];
            }

            $datum['commerce_insurance'] = $commerceInsurance = empty($datum['commerce_insurance']) ? 0 : $datum['commerce_insurance'];
            $datum['traffic_insurance'] = $trafficInsurance = empty($datum['traffic_insurance']) ? 0 : $datum['traffic_insurance'];
            $datum['tax'] = $tax = empty($datum['tax']) ? 0 : $datum['tax'];
            $earningPersent = empty($datum['earning_persent']) ? 0 : $datum['earning_persent'];
            $datum['earning'] = round(($commerceInsurance * $earningPersent) * 0.01);
            $datum['total_money'] = round($commerceInsurance + $trafficInsurance + $tax);
            $datum['top_money'] = round($commerceInsurance * $top * 0.01);
            $datum['second_money'] = round($commerceInsurance * $second * 0.01);
            $datum['top_persent'] = $top;
            $datum['second_persent'] = $second;
            $datum['top_score'] = $datum['top_money'];
            $datum['second_score'] = $datum['second_money'];

            $userData = $userModel->where(['mobile' => $datum['sale_mobile']])->find();
            if (!empty($userData)) {
                $userData = $userData->toArray();
                $datum['sale_name'] = $userData['username'];
                if ($userData['mobile'] != $defaultMobile) {
                    if (!empty($userData['p_mobile'])) {
                        $datum['top_mobile'] = $userData['p_mobile'];
                        $scoreList[] = ['score' => $datum['top_money'], 'mobile' => $userData['p_mobile'], 'remark_content' => '一级分享收益'];
                    }
                    $pUserData = $userModel->where(['mobile' => $userData['p_mobile']])->find();
                    if (!empty($pUserData)) {
                        $pUserData = $pUserData->toArray();
                        if ($pUserData['mobile'] != $defaultMobile) {
                            if (!empty($pUserData['p_mobile'])) {
                                $datum['second_mobile'] = $pUserData['p_mobile'];
                                $scoreList[] = ['score' => $datum['second_money'], 'mobile' => $pUserData['p_mobile'], 'remark_content' => '二级分享收益'];
                            }
                        }
                    }
                }
            } else {
                continue;
            }
            $result[] = $datum;
        }

        if (!empty($scoreList)) {
            $scoreModel = model('BaoxianScore');
            foreach ($scoreList as &$v) {
                $data = $userModel->where(['mobile' => $v['mobile']])->find()->toArray();
                $v['sale_name'] = $data['username'];
            }
            unset($v);
            $scoreModel->saveAll($scoreList);

            foreach ($scoreList as $item) {
                $userModel->where(['mobile' => $item['mobile']])->setInc('share_score', $item['score']);
            }
        }

        return $result;
    }

    /**
     * @param array $where
     * @return array
     */
    public static function getListByWhere($where = []) {
        $model = new self();
        if (!empty($where)) {
            $model->where($where);
        }
        $result = $model->select();
        $result = collection($result)->toArray();
        return empty($result) ? [] : $result;
    }

    /**
     * @param array $mobiles
     * @return array
     */
    public static function getDataByMobiles($mobiles = []) {
        $model = new self();
        if (!empty($mobiles)) {
            $result = $model->
            where('sale_mobile', 'in', $mobiles)
                ->where('end_time', '>', time())
                ->select();
        } else {
            $result = $model->all();
        }
        $result = collection($result)->toArray();
        return empty($result) ? [] : $result;
    }

    /**
     * @param array $mobiles
     * @return array
     */
    public static function getDataByMobilesForBirthDay($mobiles = []) {
        $model = new self();
        if (!empty($mobiles)) {
            $result = $model->
            where('sale_mobile', 'in', $mobiles)
                ->where('birthday_time', '>', 0)
                ->select();
        } else {
            $result = $model->all();
        }
        $result = collection($result)->toArray();
        return empty($result) ? [] : $result;
    }
}
