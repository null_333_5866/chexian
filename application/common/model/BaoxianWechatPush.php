<?php

namespace app\common\model;

use think\Model;

class BaoxianWechatPush extends Model {
    // 表名
    protected $name = 'baoxian_wechat_push';
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    /**
     * @param array $data
     * @return bool
     */
    public static function InsertAllPushData($data = []) {
        $model = new self();
        $model->insertAll($data);
        return true;
    }

    /**
     * @return array
     */
    public static function getAllList() {
        $model = new self();
        $result = $model->where(['is_push' => 1])->select();
        $result = collection($result)->toArray();
        return empty($result) ? [] : $result;
    }

    /**
     * @param array $update
     * @param array $where
     * @return bool
     */
    public static function updateData($update = [], $where = []) {
        $model = new self();
        $model->update($update, $where);
        return true;
    }
}
