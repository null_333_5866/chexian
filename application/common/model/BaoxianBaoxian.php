<?php

namespace app\common\model;

use think\Model;

class BaoxianBaoxian extends Model
{
    // 表名
    protected $name = 'baoxian_baoxian';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    
    // 追加属性
    protected $append = [

    ];

    /**
     * 读取分类类型
     * @return array
     */
    public static function getCategoryTypeIndexList()
    {
        $typeList = config('site.categorytype');
        foreach ($typeList as $k => &$v)
        {
            $v = __($v);
        }
        return $typeList;
    }

    public static function getCategoryTypeList()
    {
        $typeList = config('site.categorytype');
        foreach ($typeList as $k => $v)
        {
            $list[$v] = __($v);
        }
        return $list;
    }

    public static function getBaoxianTypeList()
    {
        $typeList = config('site.baoxiantype');
        foreach ($typeList as $k => &$v)
        {
            $v = __($v);
        }
        return $typeList;
    }
    
    public function company()
    {
        return $this->belongsTo('BaoxianCompany', 'company_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
