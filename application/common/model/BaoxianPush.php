<?php

namespace app\common\model;

use think\Model;

class BaoxianPush extends Model {
    // 表名
    protected $name = 'baoxian_push';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;

    // 追加属性
    protected $append = [
        'status_text'
    ];


    public function getStatusList() {
        return ['normal' => __('Normal'), 'hidden' => __('Hidden')];
    }


    public function getStatusTextAttr($value, $data) {
        $value = $value ? $value : $data['status'];
        $list = $this->getStatusList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    /**
     * @return array|false|\PDOStatement|string|\think\Collection
     */
    public static function getPushList() {
        $model = new self();
        $list = $model->where(['status' => 'normal'])->select();
        $list = collection($list)->toArray();
        return empty($list) ? [] : $list;
    }
}
