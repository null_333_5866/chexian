<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\Area;
use app\common\model\Version;
use fast\Random;
use think\Config;
use app\common\model\BaoxianBaoxian;
use app\common\model\BaoxianBanner;
use app\common\model\BaoxianEarning;

/**
 * 公共接口
 */
class Common extends Api {

    protected $noNeedLogin = ['init', 'baoxianList', 'bannerList'];
    protected $noNeedRight = '*';
    protected $model = null;
    protected $bannerModel = null;
    protected $earningModel = null;

    public function _initialize() {
        parent::_initialize();
    }

    /**
     * 加载初始化
     *
     * @param string $version 版本号
     * @param string $lng 经度
     * @param string $lat 纬度
     */
    public function init() {
        if ($version = $this->request->request('version')) {
            $lng = $this->request->request('lng');
            $lat = $this->request->request('lat');
            $content = [
                'citydata' => Area::getCityFromLngLat($lng, $lat),
                'versiondata' => Version::check($version),
                'uploaddata' => Config::get('upload'),
                'coverdata' => Config::get("cover"),
            ];
            $this->success('', $content);
        } else {
            $this->error(__('Invalid parameters'));
        }
    }

    /**
     * 上传文件
     *
     * @param File $file 文件流
     */
    public function upload() {
        $file = $this->request->file('file');
        if (empty($file)) {
            $this->error(__('No file upload or server upload limit exceeded'));
        }

        //判断是否已经存在附件
        $sha1 = $file->hash();

        $upload = Config::get('upload');

        preg_match('/(\d+)(\w+)/', $upload['maxsize'], $matches);
        $type = strtolower($matches[2]);
        $typeDict = ['b' => 0, 'k' => 1, 'kb' => 1, 'm' => 2, 'mb' => 2, 'gb' => 3, 'g' => 3];
        $size = (int)$upload['maxsize'] * pow(1024, isset($typeDict[$type]) ? $typeDict[$type] : 0);
        $fileInfo = $file->getInfo();
        $suffix = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));
        $suffix = $suffix ? $suffix : 'file';

        $mimetypeArr = explode(',', strtolower($upload['mimetype']));
        $typeArr = explode('/', $fileInfo['type']);

        //验证文件后缀
        if ($upload['mimetype'] !== '*' &&
            (
                !in_array($suffix, $mimetypeArr)
                || (stripos($typeArr[0] . '/', $upload['mimetype']) !== false && (!in_array($fileInfo['type'], $mimetypeArr) && !in_array($typeArr[0] . '/*', $mimetypeArr)))
            )
        ) {
            $this->error(__('Uploaded file format is limited'));
        }
        $replaceArr = [
            '{year}' => date("Y"),
            '{mon}' => date("m"),
            '{day}' => date("d"),
            '{hour}' => date("H"),
            '{min}' => date("i"),
            '{sec}' => date("s"),
            '{random}' => Random::alnum(16),
            '{random32}' => Random::alnum(32),
            '{filename}' => $suffix ? substr($fileInfo['name'], 0, strripos($fileInfo['name'], '.')) : $fileInfo['name'],
            '{suffix}' => $suffix,
            '{.suffix}' => $suffix ? '.' . $suffix : '',
            '{filemd5}' => md5_file($fileInfo['tmp_name']),
        ];
        $savekey = $upload['savekey'];
        $savekey = str_replace(array_keys($replaceArr), array_values($replaceArr), $savekey);

        $uploadDir = substr($savekey, 0, strripos($savekey, '/') + 1);
        $fileName = substr($savekey, strripos($savekey, '/') + 1);
        //
        $splInfo = $file->validate(['size' => $size])->move(ROOT_PATH . '/public' . $uploadDir, $fileName);
        if ($splInfo) {
            $imagewidth = $imageheight = 0;
            if (in_array($suffix, ['gif', 'jpg', 'jpeg', 'bmp', 'png', 'swf'])) {
                $imgInfo = getimagesize($splInfo->getPathname());
                $imagewidth = isset($imgInfo[0]) ? $imgInfo[0] : $imagewidth;
                $imageheight = isset($imgInfo[1]) ? $imgInfo[1] : $imageheight;
            }
            $params = array(
                'admin_id' => 0,
                'user_id' => (int)$this->auth->id,
                'filesize' => $fileInfo['size'],
                'imagewidth' => $imagewidth,
                'imageheight' => $imageheight,
                'imagetype' => $suffix,
                'imageframes' => 0,
                'mimetype' => $fileInfo['type'],
                'url' => $uploadDir . $splInfo->getSaveName(),
                'uploadtime' => time(),
                'storage' => 'local',
                'sha1' => $sha1,
            );
            $attachment = model("attachment");
            $attachment->data(array_filter($params));
            $attachment->save();
            \think\Hook::listen("upload_after", $attachment);
            $this->success(__('Upload successful'), [
                'url' => $uploadDir . $splInfo->getSaveName()
            ]);
        } else {
            // 上传失败获取错误信息
            $this->error($file->getError());
        }
    }

    public function bannerList() {
        $this->bannerModel = model('BaoxianBanner');
        $result = $this->bannerModel
            ->where('status = "1"')
            ->order('weigh', 'desc')
            ->select();
        $this->success(__('Success'), $result);
    }

    public function baoxianList() {
        $typeArr = BaoxianBaoxian::getCategoryTypeIndexList();
        $switchArr = config('site.index_switch');
        $this->model = model('BaoxianBaoxian');
        $lists = $types = [];
        if (empty($switchArr)) {
            $types = $typeArr;
        } else {
            foreach ($typeArr as $k => $v) {
                if (!empty($switchArr[$k])) {
                    $types[$k] = $v;
                }
            }
        }

        if (!empty($types)) {
            foreach ($types as $key => $type) {
                $where = "type = '{$type}'";
                $list['key'] = $key;
                $list['label'] = $type;
                $list["list"] = $this->model
                    ->with('company')
                    ->where($where)
                    ->order(['sort' => 'desc', 'sort_by' => 'desc'])
                    ->select();
                if ($key == 'chexian') {
                    $list['key_name'] = '车型';
                } else {
                    $list['key_name'] = '险种';
                }
                $lists[] = $list;

            }
        }

        $this->success(__('Success'), $lists);
    }

    public function earningList() {
        $this->earningModel = model('BaoxianEarning');
        $result = $this->earningModel
            ->where("sale_mobile = {$this->auth->mobile}")
            ->order('start_time', 'desc')
            ->select();
        $this->success(__('Success'), $result);
    }

    public function addRemark() {
        $remarkContent = $this->request->request("remark_content");
        $earningId = $this->request->request("earning_id");
        $earningModel = model('BaoxianEarning')->where('id', $earningId)->find();
        $earningModel->remark_content = $remarkContent;
        $earningModel->save();
        $this->success(__('Success'));
    }

    public function reward() {
        $user = $this->auth->getUser();
        $allUser = model('User')
            ->where('p_mobile', $user->mobile)
            ->column('*');
        $yijiList = $erjiList = [];
        $allTotal = 0;
        if (!empty($allUser)) {
            $temp = model('BaoxianEarning')
                ->where('top_mobile', $user->mobile)
                ->group("sale_mobile")
                ->column("sale_mobile,SUM(`commerce_insurance`) as total");

            foreach ($allUser as $item) {
                $mobile = $item['mobile'];
                if ($mobile == $user->mobile) {
                    continue;
                }
                $total = empty($temp[$mobile]) ? 0 : $temp[$mobile];
                $yijiList[] = [
                    'username' => $item['username'],
                    'mobile' => $item['mobile'],
                    'total' => $total
                ];
                $allTotal += $total;
            }


            $erjiList = model('BaoxianEarning')
                ->where('second_mobile', $user->mobile)
                ->group("sale_name, top_mobile")
                ->order('total', 'desc')
                ->column("sale_name, top_mobile, SUM(`commerce_insurance`) as total");
            foreach ($erjiList as &$value) {
                $value['top_mobile'] = \app\common\model\User::getByMobile($value['top_mobile'])->username;
            }
        }

        $result = [
            'share_score' => $user->share_score,
            'share_used_score' => $user->share_used_score,
            'yiji_list' => $yijiList,
            'erji_list' => $erjiList,
            'all_total' => $allTotal
        ];

        $this->success(__('Success'), $result);
    }
}
