define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var super_admin= false;
    $.ajax({
        type: "POST",
        async:false,
        url: "/admin/baoxian/baoxian/superadmin",
        data: "name=John&location=Boston",
        success: function(msg){
            super_admin = msg=='yes'? true:false;
        }
    });

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'baoxian/company/index',
                    add_url: 'baoxian/company/add',
                    edit_url: 'baoxian/company/edit',
                    del_url: 'baoxian/company/del',
                    multi_url: 'baoxian/company/multi',
                    table: 'baoxian_company',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: super_admin,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'avatar', title: __('Avatar'), formatter: Table.api.formatter.image},
                        {field: 'image', title: __('Image'), formatter: Table.api.formatter.image},
                        {field: 'mobile', title: __('Mobile')},
                        {field: 'sort', title: __('Sort')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});