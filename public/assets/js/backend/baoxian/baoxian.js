define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var super_admin= false;
    $.ajax({
        type: "POST",
        async:false,
        url: "/admin/baoxian/baoxian/superadmin",
        data: "name=John&location=Boston",
        success: function(msg){
            super_admin = msg=='yes'? true:false;
        }
    });

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'baoxian/baoxian/index',
                    add_url: 'baoxian/baoxian/add',
                    edit_url: 'baoxian/baoxian/edit',
                    del_url: 'baoxian/baoxian/del',
                    multi_url: 'baoxian/baoxian/multi',
                    table: 'baoxian_baoxian',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'baoxian_id',
                sortName: 'baoxian_id',
                showExport: super_admin,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'baoxian_id', title: __('Baoxian_id')},
                        {field: 'company.name', title: __('Company_name')},
                        {field: 'type', title: __('Type')},
                        {field: 'detail_name', title: __('Detail_name')},
                        {field: 'rate', title: __('Rate')},
                        {field: 'sort_by', title: __('Sort_by')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});