define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var super_admin= false;
    $.ajax({
        type: "POST",
        async:false,
        url: "/admin/baoxian/baoxian/superadmin",
        data: "name=John&location=Boston",
        success: function(msg){
            super_admin = msg=='yes'? true:false;
        }
    });

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'baoxian/earning/index',
                    add_url: 'baoxian/earning/add',
                    edit_url: 'baoxian/earning/edit',
                    del_url: 'baoxian/earning/del',
                    import_url: 'baoxian/earning/import',
                    multi_url: 'baoxian/earning/multi',
                    table: 'baoxian_earning',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: super_admin,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'sale_mobile', title: __('Sale_mobile')},
                        {field: 'sale_name', title: '销售人员姓名'},
                        {field: 'customer_name', title: __('Customer_name')},
                        {field: 'business_type', title: __('Business_type')},
                        {field: 'start_time', title: __('Start_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'data_images', title: __('Data_images'), formatter: Table.api.formatter.images},
                        {field: 'birthday_time', title: __('Birthday_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'end_time', title: __('End_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'car_type', title: __('Car_type')},
                        {field: 'company_name', title: __('Company_name')},
                        {field: 'commerce_insurance', title: __('Commerce_insurance')},
                        {field: 'traffic_insurance', title: __('Traffic_insurance')},
                        {field: 'tax', title: __('Tax')},
                        {field: 'total_money', title: __('Total_money')},
                        // {field: 'top_persent', title: __('Top_persent')},
                        // {field: 'top_money', title: __('Top_money')},
                        // {field: 'second_mobile', title: __('Second_mobile')},
                        // {field: 'second_persent', title: __('Second_persent')},
                        // {field: 'second_money', title: __('Second_money')},
                        {field: 'earning', title: __('Earning')},
                        {field: 'earning_persent', title: __('Earning_persent')},
                        {field: 'remark_content', title:'备注'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});