define(['jquery', 'bootstrap', 'frontend', 'form', 'template', 'vue.min'], function ($, undefined, Frontend, Form, Template, Vue) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        index: function () {
            var value = $('input[name="row[my_images]"]').val();
            value = value === null ? '' : value.toString();
            var classname = 'img-center';
            var arr = value.split(',');
            var html = [];
            $.each(arr, function (i, value) {
                var link;
                if(value) {
                    if(i%2 == 0) {
                        link = '<div class="flex"><div class="flex-1"><a href="' + Fast.api.cdnurl(value) + '" target="_blank"><img class="' + classname + '" src="' + Fast.api.cdnurl(value) + '" /></a></div>';
                    } else {
                        link = '<div class="flex-1"><a href="' + Fast.api.cdnurl(value) + '" target="_blank"><img class="' + classname + '" src="' + Fast.api.cdnurl(value) + '" /></a></div></div>';       
                    }
                }
                html.push(link);
            });
            if (html.join('')) {
                html = '<div class="image-list">' + html.join('') + '</div>';
            } else {
                html = '<div class="image-list" style="margin-left:15px">暂无图片数据</div>';
            }
            $('.user-my-images').append(html);
        },
        login: function () {
            //本地验证未通过时提示
            $("#login-form").data("validator-options", validatoroptions);

            $(document).on("change", "input[name=type]", function () {
                var type = $(this).val();
                $("div.form-group[data-type]").addClass("hide");
                $("div.form-group[data-type='" + type + "']").removeClass("hide");
                $('#resetpwd-form').validator("setField", {
                    captcha: "required;length(4);integer[+];remote(" + $(this).data("check-url") + ", event=resetpwd, " + type + ":#" + type + ")",
                });
                $(".btn-captcha").data("url", $(this).data("send-url")).data("type", type);
            });

            //为表单绑定事件
            Form.api.bindevent($("#login-form"), function (data, ret) {
                setTimeout(function () {
                    location.href = ret.url ? ret.url : "/";
                }, 1000);
            });

            Form.api.bindevent($("#resetpwd-form"), function (data) {
                Layer.closeAll();
            });

            $(document).on("click", ".btn-forgot", function () {
                var id = "resetpwdtpl";
                var content = Template(id, {});
                Layer.open({
                    type: 1,
                    title: __('Reset password'),
                    area: ["450px", "355px"],
                    content: content,
                    success: function (layero) {
                        Form.api.bindevent($("#resetpwd-form", layero), function (data) {
                            Layer.closeAll();
                        });
                    }
                });
            });
        },
        register: function () {
            //本地验证未通过时提示
            $("#register-form").data("validator-options", validatoroptions);

            //为表单绑定事件
            Form.api.bindevent($("#register-form"), function (data, ret) {
                setTimeout(function () {
                    location.href = ret.url ? ret.url : "/";
                }, 1000);
            });
        },
        changepwd: function () {
            //本地验证未通过时提示
            $("#changepwd-form").data("validator-options", validatoroptions);

            //为表单绑定事件
            Form.api.bindevent($("#changepwd-form"), function (data, ret) {
                setTimeout(function () {
                    location.href = ret.url ? ret.url : "/";
                }, 1000);
            });
        },
        forget: function () {
            //本地验证未通过时提示
            $("#resetpwd-form").data("validator-options", validatoroptions);

            // //为表单绑定事件
            Form.api.bindevent($("#changepwd-form"), function (data, ret) {
                setTimeout(function () {
                    location.href = ret.data ? ret.data : "/";
                }, 1000);
            });
        },
        mobilelogin: function () {
            //本地验证未通过时提示
            $("#mobile-form").data("validator-options", validatoroptions);

            // //为表单绑定事件
            Form.api.bindevent($("#mobile-form"), function (data, ret) {
                setTimeout(function () {
                    location.href = ret.data ? ret.data : "/";
                }, 1000);
            });
        },
        profile: function () {
            // 给上传按钮添加上传成功事件
            $("#plupload-avatar").data("upload-success", function (data) {
                var url = Fast.api.cdnurl(data.url);
                $(".profile-user-img").prop("src", url);
                Toastr.success(__('Upload successful'));
            });
            Form.api.bindevent($("#profile-form"));
            $(document).on("click", ".btn-change", function () {
                var that = this;
                var id = $(this).data("type") + "tpl";
                var content = Template(id, {});
                Layer.open({
                    type: 1,
                    title: "修改",
                    area: ["400px", "250px"],
                    content: content,
                    success: function (layero) {
                        var form = $("form", layero);
                        Form.api.bindevent(form, function (data) {
                            location.reload();
                            Layer.closeAll();
                        });
                    }
                });
            });
        },
        sharing: function() {
            var loc = new Vue({
                el: '#content-container',
                data: {
                    result: {}
                },
                mounted: function() {
                    this.getRequestData();
                },
                methods: {
                    getRequestData: function() {
                        var self = this;
                        jQuery.ajax({
                            type: "get",
                            url: "/api/common/reward",/*url写异域的请求地址*/
                            dataType: "json",/*加上datatype*/
                            success: function(data){
                                if (data.code) {
                                    self.result = data.data;
                                    jQuery(".panel.panel-default").show();
                                    if (!data.data.yiji_list.length) {
                                        // jQuery(".earning-col-no.yiji").show();
                                    }
                                    if (!data.data.erji_list.length) {
                                        // jQuery(".earning-col-no.erji").show();
                                    }
                                }
                            },
                            error: function(xhr, status, error){
                                console.log(error);
                            },
                        });
                    }
                }
            });
        }
    };
    return Controller;
});