<?php

namespace addons\alisms;

use think\Addons;
use \Yunpian\Sdk\YunpianClient;

/**
 * Alisms
 */
class Alisms extends Addons
{

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }

    /**
     * 短信发送行为
     * @param   Sms     $params
     * @return  boolean
     */
    public function smsSend(&$params)
    {
        $config = get_addon_config('alisms');

        //初始化client,apikey作为所有请求的默认值
        $clnt = YunpianClient::create($config['APIKEY']);
        $param = [YunpianClient::MOBILE => $params->mobile, YunpianClient::TEXT => "【{$config['yunpian_sign']}】您的验证码是{$params->code}"];
        $r = $clnt->sms()->single_send($param);
        return $r->isSucc();
    }

    /**
     * 短信发送通知
     * @param   array   $params
     * @return  boolean
     */
    public function smsNotice(&$params)
    {
        $alisms = library\Alisms::instance();
        $result = $alisms->mobile($params['mobile'])
                ->template($params['template'])
                ->param($params)
                ->send();
        return $result;
    }

    /**
     * 检测验证是否正确
     * @param   Sms     $params
     * @return  boolean
     */
    public function smsCheck(&$params)
    {
        return TRUE;
    }

}
